#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "header.h"


/* This function is the core of the model */

double * updater(const int particle_number, /* number of particles            */
	    const float material_with,      /* with of the material           */
	    const double mean_path,         /* mean free path of the material */
	    const double Ei,                /* initial energy of the neutron  */
	    const double A,                 /* Mass number of the material    */
	    const double E_thr)             /* Threshold energy               */
{
  /* seed */
  srand((unsigned int)time(NULL));

  /* Particle array */
  struct Particle *P = (struct Particle *) malloc(particle_number *
						  sizeof(struct Particle));

  /* fix the initial energy of every neutron and the position */

  int i;
  for (i=0 ; i<particle_number; i++)
      P[i].X  = displacement(RN, mean_path);

  /* array to store final energies of every particle */
  double *Efinal = (double *) malloc(particle_number * sizeof(double));

  for (i=0 ; i<particle_number ; i++)
    {
      double alpha;
      double tmp_X = P[i].X;
      double tmp_E = Ei;
      /* follow the path of every single neutron */
      while(tmp_X <= material_with && tmp_X >= 0.0)
	{
	  alpha = angle(RN);
	  tmp_X += cos(alpha) * displacement(RN, mean_path);
	  tmp_E = final_energy(tmp_E, A, alpha);
	  if (tmp_E <= E_thr)
	    {
	      tmp_E = 0.0;
	      break;
	    }
	}
      Efinal[i] = Ei - tmp_E;
    }

  /* Deallocate memory */
  free(P);

  return Efinal;
}

inline double displacement(const double random_number, const double mean_path)
{
  return -log(random_number) * mean_path;
}

inline double angle(const double random_number)
{
  return acos(1.0 - 2.0 * random_number);
}
/*
inline double final_energy(const double E, const double A, const double alpha)
{
  return -99.0;
}
*/
