#include "header.h"

int main(int argc, char** argv)
{
  if(argc==1)
    {
      printf("\n\tUsage ./executable [number of particles] ...\n\n");
      return(-1);
  }

  const int number_particles = atoi(argv[1]);
  
  double *tmp = updater(number_particles,
  			1.0,
  			1.0,
  			1.0,
  			1.0,
  			0.1);
  int i;
  for( i=0; i < number_particles; i++){
       printf("Energy value = %f \n",tmp[i]);
  }
  


  return 0;
}
